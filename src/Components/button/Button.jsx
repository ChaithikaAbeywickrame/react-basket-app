
import './Button.css'
import React from 'react'

const Button = (props) => {
    let {text, onClick, disableBtn} = props
    let btnType = `btn btn-${props.color}`;
    let btnDisable = (disableBtn) ? 'disabled' : '';

    return (
        <input type="button" className={btnType} disabled={btnDisable} value={text} onClick={onClick}/>
    );
}

export default Button;
