import './Basket.css'

import React, { useState, useEffect } from 'react';

import Button from '../button/Button'

const Basket = () => {

    let [basketItems, setBasketItems] = useState(0);
    let [rmBtnDisable, setRmBtnDisable] = useState(false);
    let [addBtnDisable, setAddBtnDisable] = useState(false);

    // useEffect(()=> {
    //     if(basketItems === 5){
    //         setAddBtnDisable(true);
    //     }else{
    //         setAddBtnDisable(false);
    //     }
    //     if(basketItems === 0){
    //         setRmBtnDisable(true);
    //     }else{
    //         setRmBtnDisable(false);
    //     }
    // },[basketItems])

    const handleAdd = () => {
        setBasketItems(items =>  items < 5 ? items + 1: items )
    }
    const handleRemove = () => {
        setBasketItems(items =>  items !== 0 ? items - 1: items )
    }
    return (
        <div>
            <div className="basket-image">
                <div className="item-number">{basketItems}</div>
            </div>
            <Button text={'Add Item'} color={'primary'} disbleBtn={addBtnDisable} basketItems={basketItems} btnType="add" onClick={handleAdd} />
            <Button text={'Remove Item'} color={'warning'} disableBtn={rmBtnDisable} basketItems={basketItems} btnType="remove" onClick={handleRemove} />
        </div>
    );
}

export default Basket;